import campominado.Bomba;
import campominado.Campominado;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Teste {
    
    public Teste() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testeMatriz1() {
        String [][]matriz = {{"*","1","0","0"}, {"2","2","1","0"}, {"1","*","1","0"}, {"1","1","1","0"}};
        ArrayList<Bomba> bombas = new  ArrayList<Bomba>();
        bombas.add(new Bomba(0,0));
        bombas.add(new Bomba(3,2));
        assertEquals(matriz, Campominado.gerar(4, 4, bombas));
    }
    
}
